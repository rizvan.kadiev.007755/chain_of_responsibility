from abc import ABC, abstractmethod


class RequestHandler(ABC):
    def __init__(self, successor=None):
        self._successor = successor

    @abstractmethod
    def handle_request(self, request):
        pass


class AuthenticationHandler(RequestHandler):
    def handle_request(self, request):
        if request.get("Authenticated"):
            print("Пользователь аутентифицирован")
            if self._successor:
                self._successor.handle_request(request)
        else:
            print("Ошибка аутентификации")


class AuthorizationHandler(RequestHandler):
    def handle_request(self, request):
        if request.get("Authorized"):
            print("Пользователь авторизован")
            if self._successor:
                self._successor.handle_request(request)
        else:
            print("Доступ запрещен")


class LoggingHandler(RequestHandler):
    def handle_request(self, request):
        print("Запись в журнале: ", request)
        if self._successor:
            self._successor.handle_request(request)


class DataProcessingHandler(RequestHandler):
    def handle_request(self, request):
        print("Обработка данных запроса: ", request)
        if self._successor:
            self._successor.handle_request(request)
