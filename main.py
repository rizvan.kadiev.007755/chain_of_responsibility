from chain import *

if __name__ == "__main__":
    # Создаем цепь обработчиков
    auth_handler = AuthenticationHandler()
    authz_handler = AuthorizationHandler()
    log_handler = LoggingHandler()
    data_handler = DataProcessingHandler()

    # Связываем обработчики в цепочку
    auth_handler._successor = authz_handler
    authz_handler._successor = log_handler
    log_handler._successor = data_handler

    # Создаем тестовый запрос
    request = {
        "Authenticated": True,
        "Authorized": True,
        "Data": "данные",
    }

    # Передаем запрос на обработку
    auth_handler.handle_request(request)
